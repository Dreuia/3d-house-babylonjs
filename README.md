Cara menjalankan project Babylon:

1. Buka Aplikasi Visual Studio Code
2. Install Extension Live Server (publisher:"Ritwick Dey")
3. Setelah extension sukses terinstall, open folder projek babylon js yang ingin dijalankan
4. Lalu dibagian kanan bawah aplikasi Visual Studio Code (bottom bar), terdapat tombol Go Live. Tekan saja tombol tersebut, maka projek akan dijalankan pada browser komputer anda.
